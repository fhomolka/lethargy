 
#include "engine.h"
#include "spawns.h"

#include <stdio.h>
#include "raylib.h"

int main(void) {
    
    actor_t* act = make_test_actor();

    engine_init();

    engine_loop();
    CloseWindow();

    printf("Actor has symbol %c and health %d\n", act->symbol, act->health);

    return 0;
}
