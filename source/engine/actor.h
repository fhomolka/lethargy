#ifndef LETHARGY_ACTOR_H
#define LETHARGY_ACTOR_H
#include "raylib.h"

struct actor_s
{
    Vector2 position;
    char symbol;
    int health;

    //function pointers
    void (*ready)(struct actor_s* self);
    void (*think)(struct actor_s* self);
    void (*die)(struct actor_s* self);

    //stuff used by the management
    int _active;
};

typedef struct actor_s actor_t;

extern int used_actors_in_level;
extern actor_t actors[255];

actor_t* make_actor();
actor_t* make_actor_with_values(char symbol, int health);
actor_t* make_actor_with_values_and_funcs(char symbol, int health, void (*ready)(actor_t*), void (*think)(actor_t*), void (*die)(actor_t*));

void actors_think();

#endif