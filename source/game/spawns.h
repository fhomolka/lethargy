#ifndef SPAWNS_H
#define SPAWNS_H

#include "actor.h"

void test_actor_think(actor_t* self);
actor_t* make_test_actor();

#endif