#include "actor.h"
#include "raylib.h"
#include <stdlib.h>
#include <stdio.h>

void test_actor_think(actor_t* self)
{
    static char hp_text[4];

    self->position.y += (IsKeyDown(KEY_S) - IsKeyDown(KEY_W));
    self->position.x += (IsKeyDown(KEY_D) - IsKeyDown(KEY_A));

    if (self->health > 0)
    {
        
        

        self->health -= 1;
        sprintf(hp_text, "%d", self->health);

        
        DrawText(hp_text, self->position.x, self->position.y + 11, 10, RAYWHITE);
    }
    DrawText(&self->symbol, self->position.x, self->position.y, 10, RAYWHITE);
}

actor_t* make_test_actor()
{
    actor_t* new_actor = make_actor_with_values_and_funcs('A', 100, 0, test_actor_think, 0);

    return new_actor;
}