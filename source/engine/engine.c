#include "raylib.h"
#include "config_lethargy.h" //TODO(fhomolka): load config set in CMake or something
#include "actor.h"

void engine_init() {
    InitWindow(SCREEN_WIDTH, SCREEN_HEIGHT, GAME_TITLE);
    SetTargetFPS(TARGET_FPS);
}

void engine_loop() {
    while (!WindowShouldClose())
    {
        actors_think();
        BeginDrawing();    
        ClearBackground(BLACK);
        EndDrawing();
    }
}