#include "actor.h"

int used_actors_in_level;
actor_t actors[255];

actor_t* make_actor()
{
    if (used_actors_in_level >= 255)
    {
        return ((void *)0);
    }

    actor_t* newActor = &actors[used_actors_in_level++];
    newActor->_active = 1;

    return newActor;
}

actor_t* make_actor_with_values(char symbol, int health)
{
    actor_t* newActor = make_actor();
    if (newActor == (void *)0)
    {
        return newActor;
    }

    newActor->symbol = symbol;
    newActor->health = health;
    
    return newActor;
}

actor_t* make_actor_with_values_and_funcs(char symbol, int health, void (*ready)(actor_t*), void (*think)(actor_t*), void (*die)(actor_t*))
{
    actor_t* newActor = make_actor_with_values(symbol, health);
    if (newActor == (void *)0)
    {
        return newActor;
    }

    newActor->ready = ready;
    newActor->think = think;
    newActor->die = die;
}

void actors_clear()
{
    for (unsigned int i = 0; i < 255; i++)
    {
        actors[i].health = 0;
        actors[i].ready = 0;
        actors[i].think = 0;
        actors[i].die = 0;
        actors[i]._active = 0;
    }
    
}

void actors_think()
{
    for (unsigned int i = 0; i < used_actors_in_level; i++)
    {
        if (!actors[i]._active) continue;
        if (actors[i].think == 0) continue;
        
        actors[i].think(&actors[i]);
    }
}