#ifndef LETHARGY_CONFIG_H
#define LETHARGY_CONFIG_H

const int SCREEN_WIDTH = 640;
const int SCREEN_HEIGHT = 480;
const int TARGET_FPS = 24;

const char* GAME_TITLE = "Lethargy";

#endif